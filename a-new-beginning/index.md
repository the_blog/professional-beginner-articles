---
title: "A New Beginning"
tags: ["featured"]
date: "2019-05-25"
---

Two and a half year after the last update, I still considered this blog to be well and alive, simply experiencing a slight hiatus. Now, I see how, maybe, it was dead all along. That realization was key to allow for what's going to happen next.<!--end-->

#### This Blog is now ready for a Rebirth


## A Need for Change
_"I will not wait to be an expert on a topic to publish my thoughts"_. This is the very first point in my manifesto, a prominent concept in the very nature of this blog, and yet... It was more wishful thinking than anything else. I didn't _actually_ wait to be an expert on a topic to start writing, but I _did_ not allow a lot of room for error. I wished for this blog to be a place of free expression, where I could simply write about my vision on software, explore different path, get feeedback and start discussions. But, in reality, none of that could happen.


When I first started this blog, my life was different, my perception of my passion and carreer was different, and I was a different person. I knew I couldn't make things perfect, but I still believed I owed it to myself to explore a topic in as many details as possible before publishing any related content online. And here's the catch: You **can't** _wait_ to explore _"as many details as possible"_ before publishing anything, because... there is no limit to that inquiry, it's practially infinite. And so, blog post would end up taking dozens of hours of my time, each time turning what had been a pleasure to start into a burden I had to complete. In the end, I am proud of the result, there are few article, but in each and every one of them I put as much of myself as I possibly could. And **that**, maybe, is the core of the problem.

Dedicating a enormous amount of time and ending up with a result I genuinely feel proud of, each and every time... isn't as good as it might sound. Here is why: **You can't improve if you're trying to start perfect**. Not only is it not sustainable, the amount of work is dispropotionate in relation to the result, it also simply isn't productive. You see, putting so much work in my articles, yes, made me "proud". But what does "proud" even mean in that context? It means: much less openen to criticism.

I always had a positive response to criticism, but being _open_ to criticism isn't only about the response. The most important aspect of it is: _"Did I fully take into account that opposite view, did I learn from it, and did I end up evolving into a slightly different person?"_.It is that evolution, and full consideration of one's opinion, that was made difficult by the attachement I developed with my articles by spending an unreasonable amount of time on them.

So, I decided to go for a slightly adjusted orientation. I will still "not wait to be an expert on a topic to publish my thoughts", but I will reduce my effort on vulgarization. The major part of the blog, if not its entirety, was focused on vulgarisation, making complex concepts simple to understand. The problem is, attempting vulgarization without actually having at least _some_ sort of expertise on a topic is an incredibly difficult feat. That is for one simple question: _"When is it fair to simplify?"_ 
I will still share my perspective on topics I found complex when I first discovered them and can now explain in simple terms, but I won't attempt the all encompassing overview I used to aim for. Instead, I would like this blog to turn more into a place for exploration, a written account of my experimentations, a place to share whatever I feel like sharing. In other words... a blog.

## The Road ahead
I would like for this blog to becomes a plateform of free expression, where I present things as they are. Real software development is slow, and **never** starts perfect. I want this blog to be comfortable with that.  And maybe, just maybe... help alleviate the Impostor Syndrom in the mind of the reader.

To support that refined orientation, here are a couple of ideas I had, we'll see how they all turn out:

* #### Quick intro & debrief for each and every of the side project I am working on.  
  Just a simple writeup on the reason for the project, its current state, and a debrief on my learnings so far.
* #### Quick tips and "gotchas"  
  After starting what seems to be a "simple" project, and ending up spending an unexpected amount of time yak-shaving before being able to get to work, I often end up with tips and tricks I wish I knew back when I first started. Well hopefully, that'll help the next one :)
* #### Semi-Real-Time log of my progress when working on / studying technical books.
* #### Open Source "Quick Dive"  
  Pick an open-source project, spend 2-3 days trying to understand its architecture, share how far I went. No matter if I ended up being comfortable with the project or not. The goal would be to see how far I could go in a pre-determined span of time.
* #### And probably more...  
  As mentionned, here is now a place for exploration, so who knows what the future holds.
  
And now, for my very first project in this new era, I will spend the next couple of weeks refreshing the blog engine doing all the magic behind the scenes, and learn a bit more of Front-End development by the same occasion. I will, naturally, write about my experience here.

Oh and, one more thing: this blog will now revert to its original name. Welcome to "The Professional Beginner", where we apply a ["beginner's mind"](https://en.wikipedia.org/wiki/Shoshin) to the world of software.

*Until next time --- Florian Kempenich, The Professional Beginner*

> The beach picture associated with this post, is actually the place where I finished writing it. In beautiful Lagos, Portugal 😍
