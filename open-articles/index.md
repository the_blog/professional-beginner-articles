---
title: "Open Articles"
tags: ["featured"]
date: "2016-05-28"
---

In this very short article, I want to present the concept of **Open Articles**. What they **are**, how are they **different** from other articles, why the **name**?<!--end-->

##The informed beginner
Being the **Professional Beginner**, I clearly state that I am **not** an **expert** on most the topics I talk about. This allow me to blog in **real time** on topics I am currently **studying** and sharing my **latest findings** with **you**.

However, I try as much as possible to at least **know** what I am talking about. It is very **possible** that my articles do not depict the **complete** picture of a specific topic. But at least I try to get my **information** as **correct** as possible. And by any means, if I get something **wrong**, do not hesitate to **correct** me: That's the whole **purpose** of this blog.

##The naive student
While this is true for *regular* articles, there are times where I would really love to hear some **feedback** on a **topic** I know **very little** about.

Usually, these are the kind of **questions** that betray a complete **lack of experience** on a specific topic. But that's ok, **learning** always starts with first **realizing** what needs to be learned.

##Come in, We're open!
And that's the concept of **Open Articles**. These are articles where I'm **not** really sure if **got things right**, or if I completely **missed the point**.

Sometimes with **experience** some things become **obvious**, and we tend to **forget** how quirky they **looked** before we fully **grasped** their concept.

That's the goal of **Open Articles**. Maybe the questions I have, have actually **obvious** answers from your side. And if that's the case . . . **Please come in, we're open!**

*I think there is no need to mention it, especially in this article. But any comments would be very welcome. As always, thanks for your support*


*--- The Professional Beginner*
