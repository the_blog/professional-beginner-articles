---
title: "My Learning Path"
tags: ["featured"]
date: "2016-05-01"
---

Being the **Professional Beginner** I am a constant **learner**. Where do I get this knowledge from? How to follow my progress?<!--end-->

This is exactly what this article is about. I will present to you the **path** I chose to follow for the next few month, that will, I hope, bring me to a **higher level of understanding**, and provide me with a **critical view** of the **Object Oriented** Programming world.

##Turbulent sea
At the very **beginning** of my research, I was **overwhelmed!** So many resources: books, blogs, videos. So much information was available I didn't quite know what to **do** with it or where to **start**.

In addition, a lot of technical articles assume **previous knowledge** from the reader. Not necessarily technical knowledge per se. But rather a knowledge of the history of **evolution** in the use of technologies in the world of Computer science

From my **observation**, here is what **typically** happens:

- Some **new concept** is introduced
- It is supposed to **solve** a problem a lot of programmers have
- Everybody starts **using** it
- Everybody starts **abusing** it
- Everybody **complains** about it
- . . .
- Some **new concept** is introduced
- . . .

And the **loop** goes on and on. I'm sure there are very **valid reasons** for this cycle. The **needs** evolve with the available **means**. I get that. But the thing is: it made it **incredibly difficult** for me to **make sense** out of all of this.

Is this technology **A** supposed to solve **B**, or has it been introduced to solve problems present in **C**, **C** being a solution to some of the problem from **B**, with **A** reintroducing a subset of **B**'s previous problems?

This is what the industry looks like from the point of view of a beginner. On top of that come ***religion wars*** on methodology A vs B. **Literally chaotic!**

I quickly **realized** that simply browsing articles, reading blog posts, checking StackOverflow topics just wouldn't cut it. I needed a **plan of action.**

##A structured journey

Believe it or not, even in 2016, nothing beats **books** when it comes to a **concentration of knowledge** around a specific **topic**. The **problem** with the **internet** is that one thing leads to another, and another, and another. And we're back in **chaos!**

**Books** do not have this problem. Words are **set in stone**, well paper, and **do not evolve** with time. Making **books** a much easier way to get an **idea** of the **state of the industry** at a certain **point in time**. The problem with the **internet** ironically is: **It's up to date.**

Since I like to go in **depth**, and **finish** what I started, studying from books takes **time**. Therefore I really wanted to pick the ***right*** ones.

I spent a full **month** crafting a list of **books**, that has a defined **progression**, where every single one of these books help me in a **tangible** way to **move forward** to my **goal**: Having an **overview** of **Object Oriented** programming. Thus is born my roadmap: **My Learning Path**.

##My Learning Path

In this section will follow a **list** of books. These are the books that constitute my **roadmap**. Each of these achieves specific **purpose**, which will be described below. Also, will be stated the **status** of the book:

- **DONE**
    - I **read** the book, and **used** its teaching in **multiple** examples. I think I **integrated** its principles. At least **enough** to move on for now. This doesn't mean I wouldn't often come back to it for further information.
- **READ**
    - I **read** the book, **applied** it on some example. But I **do not** have a **sufficient understanding** of the concepts exposed to consider it **done**.
- **IN PROGRESS**
    - **Currently** working on this book
- **NEXT**
    - Is the **next** one on my list
- **BACKLOG**
    - Are scheduled for **later** use.


####[Clean Code](http://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882) by Robert Martin
######Purpose
Get the **fundamentals** of well structured and organized code. What does ***good*** code looks like? And how to achieve this goal?

######Status: **DONE**

####[Tdd By Example](http://www.amazon.com/Test-Driven-Development-Kent-Beck/dp/0321146530) by Kent Beck
######Purpose
Understand what **TDD** really is about. Where does it comes from? What problems was it supposed to solve, without all the things that got added along the way.

######Status: **DONE**

####[Practical TDD and Acceptance TDD for Java Developers](http://www.amazon.com/Test-Driven-Acceptance-Java-Developers/dp/1932394850) by Lasse Koskela
######Purpose
This book is much more **recent** than the original [Tdd By Example](#tdd-by-example-by-kent-beck), therefore it benefits from the **cumulated experience of years of TDD practice**. Knowing the good, the bad and the ugly, what are the **solutions** that **contemporary** applications use?

######Status: **DONE

####[Refactoring: Improving the Design of Existing Code](http://www.amazon.com/Refactoring-Improving-Design-Existing-Code/dp/0201485672) by Martin Fowler
######Purpose
How to **gradually** modify the structure of a whole codebase while keeping it **functioning** at the same time? That's what I'm definitely highly impatient to learn from this book.

######Status: **NEXT**

####[Refactoring to patterns](http://www.amazon.com/Refactoring-Patterns-Joshua-Kerievsky/dp/0321213351/) by Joshua Kerievsky
######Purpose
**Same goal** as with the previous book. But both have been recommended countless times. I guess there's never too much of it. It sounds fascinating anyway :)

######Status: **NEXT**

####[Agile Software Development, Principles, Patterns, and Practices](http://www.amazon.com/Software-Development-Principles-Patterns-Practices/dp/0135974445) by Robert Martin
######Purpose
In this book **Robert Martin** introduced the **SOLID** principles, among many other things. This book is considered a **reference** in Object Oriented Design. I hope this book will help me in my goal of **modular design**. But I didn't research it too much, I just assumed it's fantastic. It's **Uncle's Bob** after all.

######Status: **BACKLOG**

####[Working Effectively with Legacy Code](http://www.amazon.com/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052) by Michael Feathers
######Purpose
Having a strong **test harness** is such a great feeling, and gradual **refactoring** helps keep this experience as enjoyable as possible. But what happens when a whole production system is **not** covered by tests? Well, I hope this book will teach me more.

I decided to keep it for later because I first want to have a strong knowledge of the fundamentals. Getting familiar with the **happy path** before tackling trickier situations.

######Status: **BACKLOG**

####[The Pragmatic Programmer](http://www.amazon.com/Pragmatic-Programmer-Journeyman-Master/dp/020161622X) by Andrew Hunt and David Thomas
######Purpose
Again I heard so many **good things** about this book, I didn't want to spoil the pleasure of **discovering** it. So I **do not** know exactly what it is about, but I'm sure I will enjoy it. I scheduled it just after I finish [Clean Code](#clean-code-by-robert-martin)

######Status: **DONE**

####[Domain Driven Design](http://www.amazon.com/Domain-Driven-Design-Tackling-Complexity-Software/dp/0321125215) by Eric Evans
######Purpose
I said to **myself** when I started this adventure, that until I was **done**, I would **not** allow the list to **evolve**. It is very important to have an **end**. It allows me to measure **progress** and keep **motivation**. You can find out more about that in my "[TDD, My hopes](/tdd-my-hopes)" article.

However when starting to take a look at the **hexagonal architecture**, and trying to implement it in an Android Application: [The Tic Tac Toe Project](/hexagonal-android-pt1-intro). I was confronted with some very complex conceptual **problems** as to what should happen in the **domain** and at its border.

From what I could read, this book seems the **perfect** candidate to solve these dilemmas. So I decided **for now** to use **temporary** solutions that still make sense, and postpone the **real thinking** to when I will read this book.

So far is not scheduled at a specific, I will decide later depending on my progress on other topics.

######Status: **BACKLOG**

###A long way to go
That concludes my **roadmap**. I still have a **long way** to go before seeing the end of the tunnel. But it's an **enjoyable ride**, so that's ok.
You can also use this page to follow my **progress** if you are interested in doing so.

*If you have any thoughts on this list of books, please leave a comment below. I would love to hear from you. For this specific article, I really hope you think this list makes sense and is consistent, because it is very unlikely it will evolve before I am done with it. But still any input on the subject is welcome.*

*--- The Professional Beginner*
